const COLORS = {
  white: "#FFFFFF",
  black: "#000000",
  blue: "#5D5FEE",
  grey: "#EFECEC",
  lightGrey: "#F3F4FB",
  darkBlue: "#7978B5",
  red: "#FF3B30",

  lightTheme: {
    background: "#FFFFFF", // Main background color for light mode
    secondaryBackground: "#F1F3F4", // Secondary background color (e.g., input fields, cards)
    primaryText: "#000000", // Main text color for light mode
    secondaryText: "#80868B", // Secondary text color (e.g., placeholders, hints)
    accentGreen: "#34C759", // Accent color for success messages, highlights
    accentBlue: "#1263FB", // Accent color for buttons and links
    accentRed: "#FF3B30", // Accent color for errors and warnings
    buttonBackground: "#101010", // Background color for buttons
    buttonText: "#FFFFFF", // Text color for buttons
    linkText: "#0E1013", // Text color for links
    successBackground: "#EBF9EE", // Background color for success messages
    errorBackground: "#FFEBEA", // Background color for error messages
  },

  darkTheme: {
    background: "#17181B", // Main background color for dark mode
    secondaryBackground: "#3C4043", // Secondary background color (e.g., input fields, cards)
    primaryText: "#FFFFFF", // Main text color for dark mode
    secondaryText: "#80868B", // Secondary text color (e.g., placeholders, hints)
    accentGreen: "#34C759", // Accent color for success messages, highlights
    accentBlue: "#1263FB", // Accent color for buttons and links
    accentRed: "#FF3B30", // Accent color for errors and warnings
    buttonBackground: "#101010", // Background color for buttons
    buttonText: "#FFFFFF", // Text color for buttons
    linkText: "#F1F3F4", // Text color for links
    successBackground: "#EBF9EE", // Background color for success messages
    errorBackground: "#FFEBEA", // Background color for error messages
  },
};

export default COLORS;
