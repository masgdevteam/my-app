import React, { useState, useRef, useContext } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import { Button } from "react-native-paper";
import { Icon } from "react-native-paper";
import COLORS from "../constant/colors";
import themeContext from "../context/themeContext";

const PaySlipPinScreen = ({ navigation }) => {
  const theme = useContext(themeContext);
  const [pin, setPin] = useState(["", "", "", "", "", ""]); // State to hold PIN values
  const [error, setError] = useState(false); // State to handle incorrect PIN error
  const inputs = useRef([]); // Ref to keep track of input elements

  // Function to handle changes in input fields
  const handleChange = (text, index) => {
    const newPin = [...pin];
    newPin[index] = text;
    setPin(newPin);
    if (text !== "" && index < 5) {
      inputs.current[index + 1].focus(); // Move focus to next input field
    }
  };

  // Function to handle backspace key press
  const handleBackspace = (index) => {
    if (index > 0) {
      const newPin = [...pin];
      for (let i = index - 1; i < newPin.length; i++) {
        newPin[i] = "";
      }
      setPin(newPin);
      inputs.current[index - 1].focus(); // Move focus to previous input field
    }
  };

  // Function to handle form submission
  const handleSubmit = () => {
    // Convert PIN array to string for comparison
    const enteredPin = pin.join("");
    const userPin = "123456"; // Assuming user's PIN is "123456"

    // Check if entered PIN matches user's PIN
    if (enteredPin === userPin) {
      console.log("Correct PIN"); // Correct PIN
      setError(false); // Reset error state
      navigation.navigate("SalarySlipScreen");
    } else {
      console.log("Incorrect PIN"); // Incorrect PIN
      setError(true); // Set error state to true
      // Reset PIN input fields
      setPin(["", "", "", "", "", ""]);
      // Set focus on the first input field
      inputs.current[0].focus();
    }
  };

  return (
    <View
      style={[styles.container, { backgroundColor: theme.ternaryBackground }]}
    >
      {/* Title */}
      <Text style={[styles.heading, { color: theme.color }]}>Payslip</Text>

      {/* PIN Entry Instructions */}
      <View style={[styles.iconContainer, { backgroundColor: theme.black }]}>
        <Icon
          source="form-textbox-password"
          backgroundColor="black"
          color={COLORS.white}
          size={70}
        />
      </View>
      <Text style={[styles.headertext, { color: theme.color }]}>Enter PIN</Text>
      <Text style={[styles.text, { color: theme.color }]}>
        For security purposes, before accessing your payslip, you need to enter
        your PIN
      </Text>

      {/* PIN Input Fields */}
      <View style={styles.inputContainer}>
        {[...Array(6)].map((_, index) => (
          <TextInput
            key={index}
            ref={(el) => (inputs.current[index] = el)}
            style={[
              styles.input,
              {
                backgroundColor: theme.ternaryBackground,
                borderColor: theme.color,
                color: theme.color,
              },
              error && { borderColor: theme.accentRed },
            ]}
            maxLength={1}
            keyboardType="numeric"
            onChangeText={(text) => handleChange(text, index)}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === "Backspace") {
                handleBackspace(index);
              }
            }}
            value={pin[index] ? "*" : ""}
          />
        ))}
      </View>

      {/* Error Message for Incorrect PIN */}
      {error && (
        <Text style={[styles.errorMessage, { color: theme.accentRed }]}>
          Incorrect PIN. Please try again.
        </Text>
      )}

      {/* Submit Button */}
      <Button
        mode="contained"
        onPress={handleSubmit}
        style={{
          backgroundColor: theme.buttonBackground,
          width: "50%",
          marginTop: 10,
        }}
      >
        Submit
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 20,
  },
  heading: {
    fontSize: 30,
    fontWeight: "bold",
    marginBottom: 20,
    color: COLORS.black,
  },
  iconContainer: {
    marginBottom: 10,
    width: 140,
    height: 140,
    backgroundColor: "black",
    borderRadius: 70,
    justifyContent: "center",
    alignItems: "center",
  },
  headertext: {
    marginBottom: 10,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 16,
  },
  text: {
    marginBottom: 10,
    textAlign: "center",
    fontWeight: "300",
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    padding: 10,
    marginHorizontal: 5,
    width: 40,
    textAlign: "center",
    borderRadius: 10,
  },

  button: {
    backgroundColor: COLORS.black, // Change button color to black

    borderRadius: 10,
  },
  buttonText: {
    color: COLORS.white,
    textAlign: "center",
    fontSize: 18,
    fontWeight: "bold",
  },
  errorMessage: {
    marginBottom: 10,
  },
});

export default PaySlipPinScreen;
