import React, { useState, useEffect } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { DarkTheme, DefaultTheme } from "@react-navigation/native";
import HomeScreen from "./HomeScreen";
import ActivityScreen from "./ActivityScreen";
import AttendenceScreen from "./AttendenceScreen";
import AnalyticScreen from "./AnalyticScreen";
import ProfileScreen from "./ProfileScreen";
import { FontAwesome } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { MaterialIcons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import theme from "../context/theme";
import themeContext from "../context/themeContext";
import { EventRegister } from "react-native-event-listeners";
import { useColorScheme } from "react-native";

const Tab = createBottomTabNavigator();

const Dashboard = () => {
  const colorScheme = useColorScheme();
  let systemColor;
  if (colorScheme === "light") {
    systemColor = false;
    console.log(systemColor);
  } else {
    systemColor = true;
    console.log(systemColor);
  }
  const [darkMode, setDarkMode] = useState(systemColor);
  useEffect(() => {
    const listener = EventRegister.addEventListener("ChangeTheme", (data) => {
      setDarkMode(data);
    });
    return () => {
      EventRegister.removeAllListeners(listener);
    };
  }, [darkMode]);

  return (
    <themeContext.Provider value={darkMode === true ? theme.dark : theme.light}>
      <Tab.Navigator
        screenOptions={{
          tabBarActiveTintColor: "blue",
          tabBarInActiveTintColor: "grey",
          headerShown: false,
        }}
        theme={darkMode === true ? DarkTheme : DefaultTheme}
      >
        <Tab.Screen
          name="Home"
          component={HomeScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <FontAwesome name="home" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Analytic"
          component={AnalyticScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Feather name="pie-chart" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Attend"
          component={AttendenceScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialIcons name="co-present" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Activity"
          component={ActivityScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <Feather name="activity" size={size} color={color} />
            ),
          }}
        />
        <Tab.Screen
          name="Profile"
          component={ProfileScreen}
          options={{
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="account"
                size={size}
                color={color}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </themeContext.Provider>
  );
};

export default Dashboard;
