import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  Modal,
  ScrollView,
} from "react-native";
import { Button, IconButton } from "react-native-paper";
import { Agenda } from "react-native-calendars";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const Counseling = () => {
  const [items, setItems] = useState({
    "2024-05-22": [
      {
        title: "Meeting 1",
        startTime: "12.00 PM",
        endTime: "01.00 PM",
        description: "you need to presence through this app",
        date: "2024-05-22",
      },
    ],
    "2024-05-23": [
      {
        title: "Meeting 2",
        startTime: "12.00 PM",
        endTime: "01.00 PM",
        description: "Meeting with Sam",
        date: "2024-05-23",
      },
    ],
    "2024-05-24": [],
    "2024-05-25": [
      {
        title: "Meeting 3",
        startTime: "12.00 PM",
        endTime: "01.00 PM",
        description: "Team Sync-up",
        date: "2024-05-25",
      },
    ],
  });

  return (
    <SafeAreaView style={styles.container}>
      <Agenda
        items={items}
        renderItem={(item, isFirst) => (
          <TouchableOpacity style={styles.item}>
            <View style={styles.itemHeader}>
              <View
                style={{
                  width: 5,
                  borderRadius: 10,
                  backgroundColor: "#1263FB",
                }}
              />
              <View style={{ flexDirection: "column", marginRight: 10 }}>
                <Text style={styles.titleText}>{item.title}</Text>
                <Text style={styles.itemTime}>
                  {item.startTime} - {item.endTime}
                </Text>
              </View>
              <IconButton
                icon="alarm"
                iconColor="red"
                size={20}
                onPress={() => console.log("Alarm Pressed")}
                style={{ backgroundColor: "#FFEBEA", borderRadius: 10 }}
              />
            </View>

            <Text style={styles.descriptionText}>{item.description}</Text>
          </TouchableOpacity>
        )}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: "white",
    flex: 1,
    borderRadius: 10,
    padding: 15,
    marginRight: 10,
    marginTop: 17,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  itemHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  titleText: {
    color: "black",
    fontSize: 20,
    fontWeight: "600",
  },

  itemTime: {
    backgroundColor: "#E7EFFF",
    borderRadius: 15,
    color: "#1263FB",
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginTop: 5,
    fontSize: 18,
  },
  descriptionText: {
    color: "#9AA0A6",
    fontSize: 15,
    fontWeight: "300",
  },
});

export default Counseling;
