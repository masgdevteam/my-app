import React, { useState } from "react";
import { View, StyleSheet, SafeAreaView } from "react-native";
import { Button } from "react-native-paper";
import { Calendar, Timeline } from "react-native-calendars";

const CalendarScreen = () => {
  const [selectedDate, setSelectedDate] = useState(""); // State to store selected date

  const handleDayPress = (day) => {
    setSelectedDate(day.dateString); // Update selected date
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        {/* Calendar on the top */}
        <Calendar style={styles.calendar} onDayPress={handleDayPress} />
        {/* Timeline */}
        <Timeline style={styles.timeline} selectedDate={selectedDate} />
      </View>
      {/* Button on the bottom */}
      <View style={{ paddingBottom: 20 }}>
        <Button
          mode="contained"
          buttonColor="black"
          onPress={() => {
            console.log("Button");
          }}
        >
          Create Event
        </Button>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  calendar: {
    // Add any custom styles for the calendar
  },
  timeline: {
    // Add any custom styles for the timeline
  },
});

export default CalendarScreen;
