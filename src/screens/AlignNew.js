import React, { useState } from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Text,
  TouchableOpacity,
} from "react-native";
import { Button, TextInput, Modal, IconButton } from "react-native-paper";
import { Calendar } from "react-native-calendars";
import { Timeline } from "react-native-calendars"; // Ensure you are importing Timeline from the correct package
import DateTimePickerModal from "react-native-modal-datetime-picker";

const CalendarScreen = () => {
  const [selectedDate, setSelectedDate] = useState(""); // State to store selected date
  const [modalVisible, setModalVisible] = useState(false);
  const [events, setEvents] = useState([]); // State to store all events
  const [newEvent, setNewEvent] = useState({
    title: "",
    date: "",
    startTime: "",
    endTime: "",
    description: "",
  });
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isStartTimePickerVisible, setStartTimePickerVisibility] =
    useState(false);
  const [isEndTimePickerVisible, setEndTimePickerVisibility] = useState(false);

  const handleDayPress = (day) => {
    setSelectedDate(day.dateString); // Update selected date
  };

  const handleConfirmDate = (date) => {
    setNewEvent({ ...newEvent, date: date.toISOString().split("T")[0] });
    hideDatePicker();
  };

  const handleConfirmStartTime = (time) => {
    setNewEvent({
      ...newEvent,
      startTime: time.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit",
      }),
    });
    hideStartTimePicker();
  };

  const handleConfirmEndTime = (time) => {
    setNewEvent({
      ...newEvent,
      endTime: time.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit",
      }),
    });
    hideEndTimePicker();
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const showStartTimePicker = () => {
    setStartTimePickerVisibility(true);
  };

  const hideStartTimePicker = () => {
    setStartTimePickerVisibility(false);
  };

  const showEndTimePicker = () => {
    setEndTimePickerVisibility(true);
  };

  const hideEndTimePicker = () => {
    setEndTimePickerVisibility(false);
  };

  const addEvent = () => {
    const event = {
      ...newEvent,
      id: events.length.toString(), // Unique ID for each event
    };
    setEvents([...events, event]);
    setModalVisible(false);
    setNewEvent({
      title: "",
      date: "",
      startTime: "",
      endTime: "",
      description: "",
    });
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1 }}>
        {/* Calendar on the top */}
        <Calendar style={styles.calendar} onDayPress={handleDayPress} />
        {/* Timeline */}
        <Timeline
          style={styles.timeline}
          selectedDate={selectedDate}
          events={events.filter((event) => event.date === selectedDate)}
        />
      </View>
      {/* Button on the bottom */}
      <View style={{ paddingBottom: 20, paddingHorizontal: 20 }}>
        <Button mode="contained" onPress={() => setModalVisible(true)}>
          Create Event
        </Button>
      </View>
      <Modal
        visible={modalVisible}
        animationType="slide"
        onDismiss={() => setModalVisible(false)}
      >
        <ScrollView contentContainerStyle={styles.modalContainer}>
          <Text style={styles.modalTitle}>Add New Event</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Title</Text>
            <TextInput
              style={styles.input}
              placeholder="Title"
              value={newEvent.title}
              onChangeText={(text) => setNewEvent({ ...newEvent, title: text })}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Date</Text>
            <TouchableOpacity
              onPress={showDatePicker}
              style={styles.datePicker}
            >
              <View style={styles.datePickerContent}>
                <Text style={styles.dateText}>
                  {newEvent.date || "Select Date"}
                </Text>
                <IconButton icon="calendar" size={20} />
              </View>
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirmDate}
              onCancel={hideDatePicker}
            />
          </View>
          <View style={styles.timeInputContainer}>
            <View style={[styles.inputContainer, { marginRight: 10 }]}>
              <Text style={styles.label}>Start Time</Text>
              <TouchableOpacity
                onPress={showStartTimePicker}
                style={styles.timePicker}
              >
                <View style={styles.timePickerContent}>
                  <Text style={styles.timeText}>
                    {newEvent.startTime || "Select Time"}
                  </Text>
                  <IconButton icon="clock" size={20} />
                </View>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isStartTimePickerVisible}
                mode="time"
                onConfirm={handleConfirmStartTime}
                onCancel={hideStartTimePicker}
              />
            </View>

            <View style={[styles.inputContainer, { marginLeft: 10 }]}>
              <Text style={styles.label}>End Time</Text>
              <TouchableOpacity
                onPress={showEndTimePicker}
                style={styles.timePicker}
              >
                <View style={styles.timePickerContent}>
                  <Text style={styles.timeText}>
                    {newEvent.endTime || "Select Time"}
                  </Text>
                  <IconButton icon="clock" size={20} />
                </View>
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isEndTimePickerVisible}
                mode="time"
                onConfirm={handleConfirmEndTime}
                onCancel={hideEndTimePicker}
              />
            </View>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.label}>Description</Text>
            <TextInput
              style={styles.input}
              placeholder="Description"
              value={newEvent.description}
              onChangeText={(text) =>
                setNewEvent({ ...newEvent, description: text })
              }
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button mode="contained" onPress={addEvent} style={styles.button}>
              Create Event
            </Button>
            <Button
              mode="text"
              onPress={() => setModalVisible(false)}
              style={styles.button}
            >
              Cancel
            </Button>
          </View>
        </ScrollView>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  calendar: {
    marginBottom: 20,
  },
  timeline: {
    flex: 1,
    marginTop: 20,
  },
  addButton: {
    backgroundColor: "#2196F3",
    borderRadius: 5,
    paddingVertical: 10,
    alignItems: "center",

    marginBottom: 20,
  },
  modalContainer: {
    flexGrow: 1,
    padding: 20,
    backgroundColor: "white",
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 20,
  },
  inputContainer: {
    marginBottom: 20,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  input: {
    backgroundColor: "white",
    marginBottom: 10,
  },
  datePicker: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    borderRadius: 5,
  },
  datePickerContent: {
    flexDirection: "row",
    alignItems: "center",
  },
  dateText: {
    fontSize: 16,
  },
  timeInputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  timePicker: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderWidth: 1,
    borderColor: "#ccc",
    padding: 10,
    borderRadius: 5,
    flex: 1,
  },
  timePickerContent: {
    flexDirection: "row",
    alignItems: "center",
  },
  timeText: {
    fontSize: 16,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
  },
  button: {
    flex: 1,
    marginHorizontal: 5,
  },
});

export default CalendarScreen;
