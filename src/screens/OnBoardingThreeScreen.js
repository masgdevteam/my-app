import React, { useState, useContext } from "react";
import {
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Text,
  ScrollView,
  StyleSheet,
} from "react-native";
import Button from "../component/Button";
import themeContext from "../context/themeContext";

const OnBoardingThreeScreen = ({ navigation }) => {
  const theme = useContext(themeContext);

  const [isHovered, setIsHovered] = useState(false);
  const value = 100;

  return (
    <SafeAreaView
      style={[styles.container, { backgroundColor: theme.ternaryBackground }]}
    >
      <ScrollView contentContainerStyle={styles.scrollContainer}>
        <View style={styles.progressBarContainer}>
          <View
            style={[
              styles.progressBar,
              {
                width: `${value}%`,
                backgroundColor: isHovered ? theme.accentBlue : theme.color,
              },
            ]}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
          />
        </View>
        <View style={styles.skipContainer}>
          <TouchableOpacity
            onPress={() =>
              navigation.reset({
                index: 0,
                routes: [{ name: "LoginScreen" }],
              })
            }
          >
            <Text style={[styles.skipText, { color: theme.skipColor }]}>
              Skip
            </Text>
          </TouchableOpacity>
        </View>
        <Image
          source={require("../assets/OnBoardingThreeScreen.png")}
          resizeMode="contain"
          style={styles.image}
        />
        <Text style={[styles.title, { color: theme.primaryText }]}>
          Seize Work-Life Balance
        </Text>
        <Text style={[styles.description, { color: theme.secondaryText }]}>
          Need a day off? Planning a holiday? We puts time-off requests at your
          fingertips. Take charge of your work-life balance with us
        </Text>
        <Button
          title="Next"
          onPress={() => navigation.navigate("LoginScreen")}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingHorizontal: 20,
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: "space-between",
  },
  progressBarContainer: {
    height: 4,
    borderRadius: 2,
  },
  progressBar: {
    height: "100%",
    borderRadius: 2,
  },
  skipContainer: {
    flexDirection: "row-reverse",
  },
  skipText: {
    textAlign: "right",
  },
  image: {
    width: "100%",
  },
  title: {
    fontWeight: "bold",
    fontSize: 30,
    textAlign: "center",
    marginBottom: 10,
  },
  description: {
    fontWeight: "400",
    fontSize: 15,
    marginBottom: 30,
    textAlign: "center",
  },
});

export default OnBoardingThreeScreen;
