import React, { useEffect, useState } from "react";
import { Text, View } from "react-native";
import NfcManager, { NfcTech } from "react-native-nfc-manager";

const NfcTestScreen = () => {
  const [isNfcSupported, setIsNfcSupported] = useState(false);
  const [isNfcEnabled, setIsNfcEnabled] = useState(false);

  useEffect(() => {
    initNfc();
    return () => {
      NfcManager.unregisterTagEvent().catch(() => 0); // Stop listening for NFC tag events
    };
  }, []);

  const initNfc = async () => {
    try {
      await NfcManager.start();
      setIsNfcSupported(true);
      NfcManager.isEnabled().then((enabled) => setIsNfcEnabled(enabled));
    } catch (ex) {
      console.warn("NFC init error", ex);
    }
  };

  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      {isNfcSupported ? (
        <>
          <Text>NFC is supported on this device.</Text>
          {isNfcEnabled ? (
            <Text>NFC is enabled.</Text>
          ) : (
            <Text>
              NFC is not enabled. Please enable NFC in your device settings.
            </Text>
          )}
        </>
      ) : (
        <Text>NFC is not supported on this device.</Text>
      )}
    </View>
  );
};

export default NfcTestScreen;
