import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { IconButton, Button } from "react-native-paper";

const AttendenceCheckScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.text}>Clock In</Text>
        <Text style={styles.clockText}>08.00 AM</Text>
        <IconButton
          icon="face-recognition"
          iconColor="white"
          style={{
            backgroundColor: "black",
            marginBottom: 20,
          }}
          size={50}
          onPress={() => console.log("Pressed")}
        />
        <Text style={styles.headerText}>Face Recognition</Text>
        <Text style={styles.paraText}>
          Ensure you are currently at the office and in well-lit surroundings
          for optimal face recognition.
        </Text>
      </View>
      <View style={styles.bottomButtonContainer}>
        <Button
          mode="contained"
          style={{
            backgroundColor: "black",
          }}
          onPress={() => console.log("Start Live Attendance Pressed")}
        >
          Start Live Attendance
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
  },
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 15,
    fontWeight: "300",
  },
  clockText: {
    color: "green",
    fontSize: 30,
    fontWeight: "bold",
    marginBottom: 20,
  },
  headerText: {
    marginTop: 10,
    fontSize: 20,
    fontWeight: "bold",
  },
  paraText: {
    marginTop: 5,
    textAlign: "center",
    marginBottom: 20,
  },
  bottomButtonContainer: {
    marginBottom: 20,
  },
});

export default AttendenceCheckScreen;
