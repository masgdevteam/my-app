import React, { useContext } from "react";
import { View, SafeAreaView, Text, StyleSheet } from "react-native";
import { Icon, Button } from "react-native-paper";

import themeContext from "../context/themeContext";

const ResignScreen = ({ navigation }) => {
  const theme = useContext(themeContext);

  return (
    <SafeAreaView
      style={[styles.container, { backgroundColor: theme.ternaryBackground }]}
    >
      <Text style={[styles.resignText, { color: theme.color }]}>Resign</Text>
      <View style={styles.centerContent}>
        <Icon source="book-cancel-outline" color={theme.color} size={200} />
        <Text style={[styles.text, { color: theme.color }]}>
          Fiuh... you haven’t submitted any resign letter. Be happy in your
          work!
        </Text>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          mode="contained"
          buttonColor={theme.buttonBackground}
          onPress={() => navigation.navigate("ResignConfirmationScreen")}
          style={styles.button}
        >
          Submit Resign Letter
        </Button>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },
  resignText: {
    fontWeight: "bold",
    marginTop: 50,
    fontSize: 20,
  },
  centerContent: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
  text: {
    textAlign: "center",
    marginTop: 10,
  },
  buttonContainer: {
    marginBottom: 30,
    width: "100%",
  },
  button: {
    width: "100%",
  },
});

export default ResignScreen;
