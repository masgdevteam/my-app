import React, { useState, useContext } from "react";
import { View, Text, StyleSheet, ScrollView, Dimensions } from "react-native";
import { Icon, Button, Checkbox } from "react-native-paper";
import COLORS from "../constant/colors";

import themeContext from "../context/themeContext";
import DefaultCheckedItem from "../component/DefaultCheckedItem";

const { height } = Dimensions.get("window");

const ResignConfirmationScreen = ({ navigation }) => {
  const theme = useContext(themeContext);
  const [checked, setChecked] = useState(false);

  return (
    <View
      style={[styles.container, { backgroundColor: theme.ternaryBackground }]}
    >
      <Text style={[styles.title, { color: theme.color }]}>
        Resign Confirmation
      </Text>

      <View style={styles.centerContent}>
        <Icon source="book-cancel-outline" color={theme.color} size={150} />
      </View>

      <View
        style={[styles.scrollView, { backgroundColor: theme.timeFrameBgColor }]}
      >
        <ScrollView contentContainerStyle={styles.scrollContent}>
          <View style={styles.content}>
            <Text style={[styles.contentTitle, { color: theme.color }]}>
              Read this carefully before submitting your resign letter
            </Text>
            <DefaultCheckedItem text="You are required to provide a notice period as stipulated in your contract." />
            <DefaultCheckedItem text="You are encouraged to participate in an exit interview to provide valuable feedback" />
            <DefaultCheckedItem text="You must return all company property, including but not limited to access cards, etc." />
            <DefaultCheckedItem text="You are required to complete the clearance procedure as outlined by the HR department." />
            <View style={styles.checkboxContainer}>
              <Checkbox
                color={theme.color}
                status={checked ? "checked" : "unchecked"}
                onPress={() => {
                  setChecked(!checked);
                }}
              />
              <Text style={[styles.checkboxText, { color: theme.color }]}>
                By submitting your resignation, you acknowledge that you have
                read and understood the terms and conditions outlined above
              </Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.buttonContainer}>
          <Button
            style={styles.button}
            mode="contained"
            onPress={() => navigation.navigate("Dashboard")}
            buttonColor={theme.buttonBackground}
          >
            Cancel
          </Button>
          <Button
            style={styles.button}
            buttonColor={theme.menuBackground}
            mode="outlined"
            textColor={theme.color}
            onPress={() => navigation.navigate("ResignFormScreen")}
          >
            Yes, I Will
          </Button>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  centerContent: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: height * 0.1,
  },
  title: {
    textAlign: "center",
    fontSize: 20,
    marginTop: height * 0.05,
  },
  scrollView: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  scrollContent: {
    flexGrow: 1,
  },
  content: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
  },
  contentTitle: {
    marginBottom: 20,
    fontWeight: "bold",
    fontSize: 20,
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
    marginTop: 10,
    alignItems: "center",
  },
  checkboxText: {
    marginLeft: 10,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  button: {
    flex: 1,
    marginHorizontal: 5,
  },
});

export default ResignConfirmationScreen;
