const theme = {
  light: {
    theme: "light",
    color: "black",
    background: "#F1F3F4", // Main background color for light mode
    secondaryBackground: "#F1F3F4", // Secondary background color (e.g., input fields, cards)
    ternaryBackground: "#F1F3F4",
    primaryText: "#000000", // Main text color for light mode
    secondaryText: "#80868B", // Secondary text color (e.g., placeholders, hints)
    accentGreen: "#34C759", // Accent color for success messages, highlights
    accentBlue: "#1263FB", // Accent color for buttons and links
    accentRed: "#FF3B30", // Accent color for errors and warnings
    buttonBackground: "#101010", // Background color for buttons
    buttonText: "#FFFFFF", // Text color for buttons
    linkText: "#0E1013", // Text color for links
    successBackground: "#EBF9EE", // Background color for success messages
    errorBackground: "#FFEBEA", // Background color for error messages
    sectioncontainer: "#FFFFFF",
    generalText: "#000000",
    overViewContainer: "#FFFFFF",
    innerOverViewContainer: "#F4F4F4",
    clockInButtonColour: "#FFFFFF",
    clockOutButtonColour: "#101010",
    menuBackground: "#FFFFFF",
    menuIconGreen: "#61C8A4",
    menuIconRed: "#FF7E78",
    menuIconOrange: "#FA9D12",
    attendenceItemBackground: "#EBF9EE", //green
    attendenceItemText: "#34C759",
    timeFrameText: "#9AA0A6",
    timeFrameTextDark: "#F8F8F8",
    timeFrameBgColor: "#FFFFFF",
    timeFrameBgColorDark: "#0E1013",
    chartButtonBgColor: "#E0FBE2",
    chartButtonText: "#34C759",
    black: "#17181B",
    searchContainer: "#9AA0A6",
    searchContainerBgColor: "#F5F5F5",
    salarySlipContainer: "#000000",
    white: "#FFFFFF",
    inputText: "#9AA0A6",
    borderColor: "blue",
    inputField: "#000000",
    inputFieldSecondary: "#A5A5A9",
    attendanceSuccessBg: "#FFFFFF",
    iconBG: "#101010",
    skipColor: "#2C2C2C",
  },
  dark: {
    white: "#FFFFFF",
    black: "#17181B",
    theme: "dark",
    color: "white",
    background: "#17181B",
    secondaryBackground: "#3C4043", // Secondary background color (e.g., input fields, cards)
    ternaryBackground: "#000000",
    primaryText: "#FFFFFF", // Main text color for dark mode
    secondaryText: "#80868B", // Secondary text color (e.g., placeholders, hints)
    accentGreen: "#34C759", // Accent color for success messages, highlights
    accentBlue: "#1263FB", // Accent color for buttons and links
    accentRed: "#FF3B30", // Accent color for errors and warnings
    buttonBackground: "#3C4043", // Background color for buttons
    buttonText: "#FFFFFF", // Text color for buttons
    linkText: "#F1F3F4", // Text color for links
    successBackground: "#EBF9EE", // Background color for success messages
    errorBackground: "#FFEBEA", // Background color for error messages
    sectioncontainer: "#202124",
    generalText: "#FFFFFF",
    overViewContainer: "#202124",
    innerOverViewContainer: "#282A2D",
    clockInButtonColour: "#2E3134",
    clockOutButtonColour: "#3C4043",
    menuBackground: "#202124",
    menuIconGreen: "#61C8A4",
    menuIconRed: "#FF7E78",
    menuIconOrange: "#FB9E12",
    attendenceItemBackground: "#282A2D", //green
    attendenceItemText: "#34C759",
    timeFrameText: "#9AA0A6",
    timeFrameTextDark: "#FFFFFF",
    timeFrameBgColor: "#202124",
    timeFrameBgColorDark: "#3C4043",
    chartButtonBgColor: "#2E3134",
    chartButtonText: "#34C759",
    searchContainer: "#9AA0A6",
    searchContainerBgColor: "#202124",
    salarySlipContainer: "#202124",
    inputText: "#FFFFFF",
    borderColor: "#FFFFFF",
    inputField: "#FFFFFF",
    inputFieldSecondary: "#BDC1C6",
    attendanceSuccessBg: "#17181B",
    iconBG: "#101010",
    skipColor: "#9AA0A6",
  },
};

export default theme;
