import React, { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {
  NavigationContainer,
  DarkTheme,
  DefaultTheme,
} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { EventRegister } from "react-native-event-listeners";
import theme from "./src/context/theme";
import themeContext from "./src/context/themeContext";
import { useColorScheme } from "react-native";

import LoginScreen from "./src/screens/LoginScreen";
import RegisterScreen from "./src/screens/RegisterScreen";
import Dashboard from "./src/screens/Dashboard";
import OnBoardingOneScreen from "./src/screens/OnBoardingOneScreen";
import OnBoardingTwoScreen from "./src/screens/OnBoardingTwoScreen";
import OnBoardingThreeScreen from "./src/screens/OnBoardingThreeScreen";
import AttendenceSuccessScreen from "./src/screens/AttendenceSuccessScreen";
import AttendenceScreen from "./src/screens/AttendenceScreen";
import ResignScreen from "./src/screens/ResignScreen";
import ResignConfirmationScreen from "./src/screens/ResignConfirmationScreen";
import ResignFormScreen from "./src/screens/ResignFormScreen";
import PaySlipPinScreen from "./src/screens/PaySlipPinScreen";
import SalarySlipScreen from "./src/screens/SalarySlipScreen";
import TimeOffScreen from "./src/screens/TimeOffScreen";
import CalendarScreen from "./src/screens/CalendarScreen";
import Overtime from "./src/screens/Overtime";
import Counseling from "./src/screens/Counseling";

const Stack = createNativeStackNavigator();

const App = () => {
  const colorScheme = useColorScheme();
  let systemColor;
  if (colorScheme === "light") {
    systemColor = false;
  } else {
    systemColor = true;
  }

  const [darkMode, setDarkMode] = useState(systemColor);
  useEffect(() => {
    const listener = EventRegister.addEventListener("ChangeTheme", (data) => {
      setDarkMode(data);
      console.log(data);
    });
    return () => {
      EventRegister.removeAllListeners(listener);
    };
  }, [darkMode]);

  return (
    <themeContext.Provider value={darkMode === true ? theme.dark : theme.light}>
      <NavigationContainer theme={darkMode === true ? DarkTheme : DefaultTheme}>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen
            name="OnBoardingOneScreen"
            component={OnBoardingOneScreen}
          />
          <Stack.Screen
            name="OnBoardingTwoScreen"
            component={OnBoardingTwoScreen}
          />
          <Stack.Screen
            name="OnBoardingThreeScreen"
            component={OnBoardingThreeScreen}
          />
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="AttendenceScreen" component={AttendenceScreen} />
          <Stack.Screen
            name="AttendenceSuccessScreen"
            component={AttendenceSuccessScreen}
          />
          <Stack.Screen name="ResignScreen" component={ResignScreen} />
          <Stack.Screen
            name="ResignConfirmationScreen"
            component={ResignConfirmationScreen}
          />
          <Stack.Screen name="ResignFormScreen" component={ResignFormScreen} />
          <Stack.Screen name="PaySlipPinScreen" component={PaySlipPinScreen} />
          <Stack.Screen name="SalarySlipScreen" component={SalarySlipScreen} />
          <Stack.Screen name="TimeOffScreen" component={TimeOffScreen} />
          <Stack.Screen name="CalendarScreen" component={CalendarScreen} />
          <Stack.Screen name="Overtime" component={Overtime} />
          <Stack.Screen name="Counseling" component={Counseling} />
        </Stack.Navigator>
      </NavigationContainer>
    </themeContext.Provider>
  );
};

export default App;
